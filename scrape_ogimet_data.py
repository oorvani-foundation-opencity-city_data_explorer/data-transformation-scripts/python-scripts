from bs4 import BeautifulSoup
import requests
import csv
import calendar

def get_data(html_text, year):
	soup = BeautifulSoup(html_text, 'lxml')
	table = soup.find_all('table')[-1]
	rows = table.find_all('tr')
	output = []		
	for row in rows[2:]:
		tds = row.find_all('td')
		date_text = tds[0].text
		max = tds[1].text
		min = tds[2].text
		if (len(tds) > 12):
		    rainfall = tds[-12].text
		else:
		    rainfall = '---'
		month, day = date_text.split('/')
		output.append(['{0}-{1}-{2}'.format(str(day), str(month), str(year)), rainfall, max, min])
	return output

days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

year = input('Enter year: ')
city = input('Enter IMD station code(find it in the dropdown called Station here: https://cdsp.imdpune.gov.in/home_riturang_sn.php): ')


base_url ='https://www.ogimet.com/cgi-bin/gsynres?lang=en&ord=REV&ndays={1}&ano={3}&mes={0}&day={1}&hora=03&ind={2}'

final_list = []

for m_index in range (0, 12):
	month = m_index + 1
	no_days = days[m_index]
	if (int(month) == 2 and calendar.isleap(int(year))):
		no_days = 29
	url = base_url.format(str(month), str(no_days), str(city), str(year))
	print('Getting month '+ str(month))
	try:
		resp = requests.get(url)
	except Exception as e:
		print('Exception thrown for url '+url+' '+e.text)
		continue
	data = get_data(resp.text, year)
	print('received len '+ str(len(data)))
	for row in reversed(data):
		final_list.append(row)

write_file = open('{0}.csv'.format(str(city)), 'a', newline='', encoding='utf-8')
csvwrite = csv.writer(write_file)
for row in final_list:
    csvwrite.writerow(row)
write_file.close()